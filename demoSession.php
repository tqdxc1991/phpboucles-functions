<?php
//VI superglobales, sessions et cookies
// Exercice 2 Sur la page index, faire un liens vers une autre page.
//  Passer d'une page à l'autre le contenu des variables nom, prenom 
//  et age grâce aux sessions. Ces variables auront été définies 
//  directement dans le code.
//  Il faudra afficher le contenu de ces variables sur la deuxième page.
session_start();
echo $_SESSION['nom'];
echo $_SESSION['prenom'];
echo $_SESSION['age'];