<?php

// Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     incrémenter de 1

// Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable

// Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     décrémenter la première variable

// Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     l'incrementer de la moitié de sa valeur

// Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

// Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

// Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

// Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
// exercise1
echo "<h1>exercise1</h1></br>";

for($i=0;$i<10;$i++){
    echo $i . " |";
}

// exercise2
echo "</br><h1>exercise2</h1></br>";


$a = 0;
$b = 50;
while( $a <= 20){
    echo $a*$b . " |";;
    $a++;
}

// exercise3
echo "</br><h1>exercise3</h1></br>";
$a = 100;
$b = 50;
while( $a > 10){
    echo $a*$b . " |";
    $a--;
}

// exercise4
echo "</br><h1>exercise4</h1></br>";
$a = 1;
while ($a<10){
    echo $a . " |";
    $a = $a + $a/2;
}

// exercise5
echo "</br><h1>exercise5</h1></br>";
$a = 1;
while($a<=15){
    echo $a . " On y arrive presque...|";
    $a = $a + 1;
}

// exercise6
echo "</br><h1>exercise6</h1></br>";
$a = 20;
while($a>=0){
    echo $a . " C'est presque bon....|";
    $a = $a - 1;
}

// exercise7
echo "</br><h1>exercise7</h1></br>";
$a = 1;
while($a<=100){
    echo $a . " On tient le bon bout...|";
    $a = $a + 15;
}

// exercise8
echo "</br><h1>exercise8</h1></br>";
$a = 200;
while($a>=0){
    echo $a . " Enfin ! ! !|";
    $a = $a - 12;
}

?>