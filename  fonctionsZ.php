<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Exercice 1 Faire une fonction qui retourne true.
echo "<h1>exercise1</h1></br>";

function returnTrue(){
    echo "true";
}
returnTrue();

echo "<br/>";

// Exercice 2 Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
echo "<h1>exercise2</h1></br>";

function writeChar(string $a){
    echo $a;
}
writeChar("Qiong");
echo "<br/>";



// Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.
echo "<h1>exercise3</h1></br>";

function addChar(string $a,string $b){
     echo $a . $b;
}
addChar("TANG","Qiong");




echo "<br/>";

// Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux
echo "<h1>exercise4</h1></br>";

function compare($a,$b){
   
    if ($a>$b){
        echo " Le premier nombre est plus grand";
    }
    else if ($a<$b){
        echo  "Le premier nombre est plus petit";
    }
    else echo "Les deux nombres sont identiques";
}
compare(100,100);
echo "<br/>";
// Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.
echo "<h1>exercise5</h1></br>";
function concatenation(string $a, int $b){
    echo $a . $b;
}
concatenation("simplon",88);
echo "<br/>";

// Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".
echo "<h1>exercise6</h1></br>";

function bonjour($nom,$prenom,$age){
    echo "Bonjour" . $nom . $prenom . ", tu as " . $age . "ans.";
}
bonjour("TANG","Qiong",29);
echo "<br/>";
// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur
echo "<h1>exercise7</h1></br>";

function genreAge($age,$genre){
    if($age>=18 & $genre=="homme"){
        echo "Vous êtes un homme et vous êtes majeur";
    }                    
         elseif($age<18 & $genre=="homme"){
            echo "Vous êtes un homme et vous êtes mineur";
         }   
         elseif($age>=18 & $genre=="femme"){
            echo "Vous êtes une femme et vous êtes majeur";
         }
         elseif($age<18 & $genre=="femme"){
            echo "Vous êtes une femme et vous êtes mineur";
         }
         else{
            return;
         }          
}
genreAge(29,"femme");
echo "<br/>";

// Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.
echo "<h1>exercise8</h1></br>";

function sum($a,$b,$c){
    $d = $a+$b+$c;
    echo $d;
}

sum (3,90,77);

echo "<br/>";


echo "<h1>tableau</h1></br>";
function afficher($a){
    echo "<p>" . $a . "</p>";
}
//exo1
$mois = [
        1 => "janvier",
        2 => "février",
        3 => "mars",
        4 =>  "avril",
        5 =>  "mai",
        6 => "juin",
        7 =>  "juillet",
        8 =>  "aout",
        9 =>  "septembre",
        10 =>  "octobre",
        11 => "novembre",
        12 =>   "decembre",
    ];
//exo2
afficher($mois[2]);

//exo3
afficher($mois[5]);

//exo4
$mois[7]="août";
var_dump($mois);
echo "<br/>";
echo "<br/>";

$mois[]="florence";
var_dump($mois);
echo "<br/>";
echo "<br/>";

for($i = 1;$i <= count($mois); $i++){
    afficher($mois[$i]);
}

foreach($mois as $key => $value){
    echo "<p>le mois de " . $value . " est de numero " . $key . "</p>";
    afficher("le mois de " . $value . " est de numero " . $key);
}

foreach($mois as $value){
    afficher($value);
}

?>

